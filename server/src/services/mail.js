'use strict'

const nodemailer = require('nodemailer');


const deliveredNumberMail = (data) => {
  console.log(Object.values(data));
  let deliveringData = Object.values(data);
  let participantEmail = deliveringData.slice(0,1).join();
  let participantNumber = deliveringData.slice(1,2).join();
  let takerName = deliveringData.slice(3,4).join();
  let takerSurname = deliveringData.slice(4).join();
  let message = 'Tu dorsal ha sido recogido! Listo para la carrera? El dorsal es el número ' + participantNumber + ' y ha sido recogido por ' + takerName + ' ' + takerSurname + '. Gracias!';
  const smtpConfig = {
    host: 'smtp.buzondecorreo.com',
    port: 465,
    secure: true, // Use SSL
    auth: {
      user: 'info@runrun.org.es',
      pass: 'Runrun11'
    }
  };
  let transporter = nodemailer.createTransport(smtpConfig);
  let mailOptions = {
    from: 'info@runrun.org.es',
    to: participantEmail, // List of receivers
    subject: 'Tu dorsal ha sido recogido!',
    text: message
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      return console.log(error);
    } else {
      return console.log(info.response);
    }
  });
}

module.exports = {
  deliveredNumberMail
};