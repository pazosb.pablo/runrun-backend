require('dotenv').config()

const fs = require('fs')
const http = require('http')
const express = require('express')
const bodyParser = require('body-parser')

const multer = require('multer')
const upload = multer({ dest: "src/storage/" });
const csv = require('csvtojson')

const cors = require('cors')
const db = require('./db')
const routes = require('./routes/routes')
const app = express();

const server = http.createServer(app);
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use('/', routes);

app.use(function (err, req, res, next) {
	console.log('This is the invalid field ->', err.field)
	next(err)
  })

// Iniciamos el servidor:
function startServer() {
  server.listen(port, () => {
    console.log("Listening on port", port);
  });
}


setImmediate(startServer);
