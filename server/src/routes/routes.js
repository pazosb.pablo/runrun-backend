'use strict'

const controller = require('../controllers/controller');
const { authenticated } = require('../middlewares/authenticated');
const express = require('express');

const router = express.Router();

const multer = require('multer');
const upload = multer({ dest: "src/storage/" });

router.put('/register/', controller.editUsers);
router.post('/register/', controller.register);
router.post('/login/', controller.login);
router.post('/events/', controller.createEvent);
router.delete('/events/:id', controller.deleteEvent);
router.delete('/organizer/:id', controller.deleteProfile);
router.get('/events/', controller.getEvents);
router.put('/events/', controller.updateEvent);
router.get('/participants/', controller.selectParticipant);
router.get('/uploadedParticipants/', controller.numberOfParticipants);
router.put('/participants/', controller.updateParticipant);
router.put('/participant/', controller.editParticipant);
router.post('/upload/', upload.single("file"), controller.uploadCsv);

module.exports = router;
